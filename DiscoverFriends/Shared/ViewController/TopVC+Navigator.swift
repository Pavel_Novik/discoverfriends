//
//  TopVC+Navigator.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

extension TopVC {

    // MARK: - PUBLIC METHODS -

    func pushPhotoDetails(photo: Photo) {
        guard let vc = UIStoryboard(name: "PhotoDetailsVC",
                                    bundle: nil).instantiateInitialViewController() as? PhotoDetailsVC else {
                                        DLog("Unable to load PhotoDetailsVC"); return
        }

        vc.photo = photo
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalTransitionStyle = .crossDissolve;
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.present(vc, animated: true, completion: nil)
    }

    func pushPostDetails(post: Post) {
        guard let vc = UIStoryboard(name: "PostDetailsVC",
                                    bundle: nil).instantiateInitialViewController() as? PostDetailsVC else {
                                        DLog("Unable to load PostDetailsVC"); return
        }

        vc.post = post
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalTransitionStyle = .crossDissolve;
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}
