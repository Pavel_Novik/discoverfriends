//
//  TopVC.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/3/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

class TopVC: UIViewController {

    // MARK: - VARIABLES -

    var screenName: ScreenNameType?

    // MARK: - INIT/DEINIT -
    deinit {
        if let screenName = self.screenName {
            DLog("***DEINIT: \(screenName.name())***")
        }
    }

}
