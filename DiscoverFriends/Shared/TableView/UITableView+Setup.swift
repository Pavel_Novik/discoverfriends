//
//  UITableView+Setup.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {

    // MARK: - PUBLIC METHODS -

    func registerCells(cellsName: [String]) {

        for cellName in cellsName {
            let cell = UINib.init(nibName: cellName, bundle: nil)
            self.register(cell, forCellReuseIdentifier: cellName)
        }
    }
    
}
