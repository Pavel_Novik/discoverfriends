//
//  TabBarController+Notifications.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation

extension TabBarController {

    // MARK: - PUBLIC METHODS -

    func addNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.notificationSessionChanged(notification:)),
                                               name: Notification.Name(kNotificationSessionChanged),
                                               object: nil)
    }

    func removeNotifications() {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - PRIVATE METHODS -

    @objc fileprivate func notificationSessionChanged(notification: NSNotification) {
        self.setupController()
    }
    
}
