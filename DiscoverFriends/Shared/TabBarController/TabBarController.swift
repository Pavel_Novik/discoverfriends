//
//  TabBarController.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/3/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

enum TabBarButtonType : Int {
    case discover = 0
    case account

    func image() -> UIImage? {
        switch self {
        case .discover: return UIImage(named: "world_icon")
        case .account:     return UIImage(named: "man_icon")
        }
    }

    func selectedImage() -> UIImage? {
        switch self {
        case .discover: return UIImage(named: "world_icon")
        case .account:     return UIImage(named: "man_icon")
        }
    }
}

class TabBarController: UITabBarController {

    // MARK: - OVERRIDE METHODS -

    override func viewDidLoad() {
        super.viewDidLoad()

        self.addNotifications()
        self.setupController()
    }

    // MARK: - INIT/DEINIT METHODS -

    deinit {
        self.removeNotifications()
    }

    // MARK: - PUBLIC METHODS -

    func setupController() {
        let isUserLoggedIn = SessionManager.sharedInstance.isUserLoggedIn()

        guard let discoverVC = UIStoryboard(name: "DiscoverVC",
                                            bundle: nil).instantiateInitialViewController() else {
            DLog("Unable to load DiscoverVC"); return
        }
        guard let accountVC = UIStoryboard(name: isUserLoggedIn ? "AccountVC" : "LogInVC",
                                           bundle: nil).instantiateInitialViewController() else {
            DLog("Unable to load Account VC"); return
        }

        let discoverNav = UINavigationController(rootViewController: discoverVC)

        // if discover already exists
        if let oldDiscoverVC = self.viewControllers?.first {
            self.setViewControllers([oldDiscoverVC, accountVC], animated: true)
        } else {
            self.setViewControllers([discoverNav, accountVC], animated: true)
        }

        guard let viewControllers = self.viewControllers else { DLog("TabBar setup error."); return }

        // Discover VC
        let discoverTabBarItem = UITabBarItem(title: "DISCOVER",
                                              image: TabBarButtonType.image(.discover)(),
                                              selectedImage: TabBarButtonType.selectedImage(.discover)())
        viewControllers[TabBarButtonType.discover.rawValue].tabBarItem = discoverTabBarItem

        // account VC
        let accountTabBarItem = UITabBarItem(title: "ACCOUNT",
                                          image: TabBarButtonType.image(.account)(),
                                          selectedImage: TabBarButtonType.selectedImage(.account)())
        viewControllers[TabBarButtonType.account.rawValue].tabBarItem = accountTabBarItem
    }
    
}
