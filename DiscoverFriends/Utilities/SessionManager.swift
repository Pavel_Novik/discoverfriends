//
//  SessionManager.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation

let kUserDefaultsUserId = "UserId"

class SessionManager {

    // MARK: - CONSTANTS -

    static let sharedInstance = SessionManager()

    // MARK: - VARIABLES -

    var user: User?

    // MARK: - PUBLIC METHODS -

    func setup() {
        if let userId = UserDefaults.standard.object(forKey: kUserDefaultsUserId) as? Int {
            self.loadUser(for: userId)
        }
    }

    func isUserLoggedIn() -> Bool {
        return self.user != nil
    }

    func logOut() {
        self.user = nil
        UserDefaults.standard.removeObject(forKey: kUserDefaultsUserId)
        NotificationCenter.default.post(name: Notification.Name(kNotificationSessionChanged), object: nil)
    }

    func loadUser(for userId: Int, completion: (() -> ())? = nil) {
        FacadeUser().getUser(for: userId, completion: { [weak self](user, serverError) in
            if serverError == false && user != nil {
                self?.user = user
                if let userId = user?.id {
                    UserDefaults.standard.set(userId, forKey: kUserDefaultsUserId)
                }
                NotificationCenter.default.post(name: Notification.Name(kNotificationSessionChanged), object: nil)
            } else {
                self?.logOut()
            }
            completion?()
        })
    }

}
