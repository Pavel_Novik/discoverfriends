//
//  Logger.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation

// Code based on http://gist.github.com/Abizern/a81f31a75e1ad98ff80d
func DLog<T>(_ object: @autoclosure () -> T, _ file: String = #file, _ function: String = #function, _ line: Int = #line){
    let fileName = file.components(separatedBy:"/").last
    let finalMessage = "DESCRIPTION:\n  FILE: \(fileName!) \n  FUNCTIONS: \(function) \n  LINE: \(line) \n  MESSAGE: \(object()) \n"

    if kDebugEnabled == true {
        print(finalMessage)
    }
}
