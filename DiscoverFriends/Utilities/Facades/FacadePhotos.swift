//
//  FacadePhotos.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FacadePhotos {

    // MARK: - PUBLIC METHODS -

    func getAllPhotos(completion: @escaping (_ photos: [Photo]?, _ serverError: Bool) -> ()) {

        let kFuncName = "GET ALL PHOTOS:"
        let endpoint = kHostName + "photos"

        func finished(_ photos: [Photo]?, _ serverError: Bool) {
            DispatchQueue.main.async {
                completion(photos, serverError)
            }
        }

        func exit() {
            DLog("\(kFuncName) Something went wrong.")
            finished(nil, true)
        }

        var requestedCount = 0

        func retry() {
            // if request failed for some reason, retry multiple times
            if requestedCount < kRequestRetryCount {
                requestedCount += 1
                let when = DispatchTime.now() + 2 // delay by 2 seconds before retry
                DispatchQueue.main.asyncAfter(deadline: when) {
                    request()
                }
            } else {
                exit()
            }
        }

        func request() {
            DLog("\(kFuncName) Request started.")

            Alamofire.request(endpoint,
                              method: .get)
                .responseJSON { response in
                    DLog("\(kFuncName) Request Ended:")

                    guard response.result.error == nil else {
                        DLog(response.result.error!.localizedDescription);
                        retry()
                        return
                    }

                    switch response.result {
                    case .success(let value):
                        DispatchQueue.global(qos: .background).async {
                            var photos = [Photo]()
                            if let jsonArray = JSON(value).array {
                                for photoJson in jsonArray {
                                    if let photoObject = Photo(json: photoJson) {
                                        photos.append(photoObject)
                                    }
                                }
                            }
                            finished(photos.count > 0 ? photos : nil, false)
                        }
                    case .failure(let error):
                        DLog(error.localizedDescription)
                        retry()
                    }
            }
        }
        
        request()
    }
    
}
