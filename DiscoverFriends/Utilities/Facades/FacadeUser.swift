//
//  FacadeUser.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FacadeUser {

    // MARK: - PUBLIC METHODS -

    func getUser(for id: Int, completion: @escaping (_ user: User?, _ serverError: Bool) -> ()) {

        let kFuncName = "GET USER:"
        let endpoint = kHostName + "users/\(id)"

        func finished(_ user: User?, _ serverError: Bool) {
            DispatchQueue.main.async {
                completion(user, serverError)
            }
        }

        func exit() {
            DLog("\(kFuncName) Something went wrong.")
            finished(nil, true)
        }

        var requestedCount = 0

        func retry() {
            // if request failed for some reason, retry multiple times
            if requestedCount < kRequestRetryCount {
                requestedCount += 1
                let when = DispatchTime.now() + 2 // delay by 2 seconds before retry
                DispatchQueue.main.asyncAfter(deadline: when) {
                    request()
                }
            } else {
                exit()
            }
        }

        func request() {
            DLog("\(kFuncName) Request started.")

            Alamofire.request(endpoint,
                              method: .get)
                .responseJSON { response in
                    DLog("\(kFuncName) Request Ended:")

                    guard response.result.error == nil else {
                        DLog(response.result.error!.localizedDescription);
                        retry()
                        return
                    }

                    switch response.result {
                    case .success(let value):
                        DispatchQueue.global(qos: .background).async {
                            finished(User(json: JSON(value)), false)
                        }
                    case .failure(let error):
                        DLog(error.localizedDescription)
                        retry()
                    }
            }
        }

        request()
    }

}
