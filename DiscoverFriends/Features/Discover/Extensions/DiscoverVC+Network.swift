//
//  DiscoverVC+Network.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation

extension DiscoverVC {

    // MARK: - PUBLICE METHODS -

    func getData( completion: @escaping () -> ()) {
        var photosRequestFinished = false
        var postsRequestFinished = false

        let downloadGroup = DispatchGroup()
        let queue = DispatchQueue.global(qos: .default)

        // wait until all requets come back
        downloadGroup.enter()
        FacadePhotos().getAllPhotos { [weak self](photos: [Photo]?, serverError: Bool) in
            self?.photos = photos
            photosRequestFinished = true
            downloadGroup.leave()
        }

        downloadGroup.enter()
        FacadePosts().getAllPosts { [weak self](posts: [Post]?, serverError: Bool) in
            self?.posts = posts
            postsRequestFinished = true
            downloadGroup.leave()
        }

        downloadGroup.notify(queue: queue) {
            if photosRequestFinished == true && postsRequestFinished == true {
                DispatchQueue.main.async {
                    completion()
                }
            }
        }
    }

}
