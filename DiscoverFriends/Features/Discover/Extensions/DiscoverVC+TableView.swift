//
//  DiscoverVC+TableView.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

enum DiscoverSectionsType: Int {
    case photos
    case posts
}

extension DiscoverVC: UITableViewDelegate, UITableViewDataSource {

    // MARK: - PUBLIC METHODS -

    func setupTableView() {
        self.tableView.estimatedRowHeight = self.tableView.frame.width
        self.tableView.registerCells(cellsName: DiscoverVCCellsType.allTitles)
        self.tableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
    }

    // MARK: - TABLE VIEW DATASOURCE/DELEGATE METHODS -

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.activeSections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.activeSections[section] {
        case .photos, .posts: return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell?

        switch self.activeSections[indexPath.section] {
        case .photos: cell = self.setupPhotosCell(for: indexPath)
        case .posts:  cell = self.setupPostsCell(for: indexPath)
        }

        return cell ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch self.activeSections[section] {
        case .photos: return 50.0
        case .posts: return 60.0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var title = ""
        switch self.activeSections[section] {
        case .photos: title = "TOP PHOTOS"
        case .posts: title = "TOP POSTS"
        }
        return self.setupHeaderCell(for: section, title: title)
    }

}
