//
//  DiscoverVC+Cells.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation

enum DiscoverVCCellsType: String {

    case sectionHeader  = "DiscoverSectionHeaderCell"
    case photosCell     = "DiscoverPhotosCell"
    case postsCell      = "DiscoverPostsCell"

    var title: String {
        return self.rawValue
    }

    static let allTitles = [sectionHeader.title,
                            photosCell.title,
                            postsCell.title]
}

extension DiscoverVC {

    // MARK: - PUBLIC METHODS -

    func setupPhotosCell(for indexPath: IndexPath) -> DiscoverPhotosCell? {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: DiscoverVCCellsType.photosCell.title,
                                                            for: indexPath) as? DiscoverPhotosCell else { return nil }
        cell.setup(with: self.photos)
        cell.tapped = { [weak self](photo: Photo) in
            self?.pushPhotoDetails(photo: photo)
        }
        return cell
    }

    func setupPostsCell(for indexPath: IndexPath) -> DiscoverPostsCell? {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: DiscoverVCCellsType.postsCell.title,
                                                            for: indexPath) as? DiscoverPostsCell else { return nil }
        cell.setup(with: self.posts)
        cell.tapped = { [weak self](post: Post) in
            self?.pushPostDetails(post: post)
        }
        return cell
    }

    func setupHeaderCell(for section: Int, title: String) -> DiscoverSectionHeaderCell? {
        guard let headerCell = self.tableView.dequeueReusableCell(withIdentifier: DiscoverVCCellsType.sectionHeader.title) as?
            DiscoverSectionHeaderCell else { return nil }
        headerCell.descriptionLabel.text = title
        return headerCell
    }
    
}
