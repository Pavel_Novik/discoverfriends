//
//  DiscoverSectionHeaderCell.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

class DiscoverSectionHeaderCell: UITableViewCell {

    // MARK: - VARIABLES -

    @IBOutlet weak var descriptionLabel: UILabel!
    
}
