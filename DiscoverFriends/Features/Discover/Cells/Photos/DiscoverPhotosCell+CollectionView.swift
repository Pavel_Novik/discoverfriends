//
//  DiscoverPhotosCell+CollectionView.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

extension DiscoverPhotosCell: UICollectionViewDelegate, UICollectionViewDataSource {

    // MARK: - PUBLIC METHODS -

    func registerCells() {
        let itemCell = UINib.init(nibName: PhotoCollectionViewCell.kCellName, bundle: nil)
        self.collectionView.register(itemCell, forCellWithReuseIdentifier: PhotoCollectionViewCell.kCellIdentifier)
    }

    // MARK: - DELEGATES/DATASOURCE METHODS -

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.kCellIdentifier,
                                                         for: indexPath) as? PhotoCollectionViewCell {
            cell.setup(with: self.photos?[indexPath.row])
            cell.tapped = self.tapped
            return cell
        }
        return UICollectionViewCell()
    }

}
