//
//  DiscoverPhotosCell.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

class DiscoverPhotosCell: UITableViewCell {

    // MARK: - VARIABLES -

    @IBOutlet weak var collectionView: UICollectionView!
    var photos: [Photo]?
    var tapped:((Photo)->())?

    // MARK: - OVERRIDE METHODS -

    override func awakeFromNib() {
        super.awakeFromNib()

        self.registerCells()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.tapped = nil
    }

    // MARK: - PUBLIC METHODS -

    func setup(with photos: [Photo]?) {
        self.photos = photos
        self.collectionView.reloadData()
    }

}
