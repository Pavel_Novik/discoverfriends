//
//  PhotoCollectionViewCell.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class PhotoCollectionViewCell: UICollectionViewCell {

    // MARK: - CONSTANTS -

    static let kCellIdentifier = "PhotoCollectionViewCellIdentifier"
    static let kCellName = "PhotoCollectionViewCell"

    // MARK: - VARIABLES -

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    fileprivate var photo: Photo?
    var tapped:((Photo)->())?

    // MARK: - OVERRIDE METHODS -

    override func prepareForReuse() {
        super.prepareForReuse()

        self.tapped = nil
    }

    // MARK: - PUBLIC METHODS -

    func setup(with photo: Photo?) {
        self.photo = photo

        self.titleLabel.text = self.photo?.title ?? ""

        if let url = self.photo?.thumbnailUrl,
            let placeholder = UIImage(named: kPlaceholderImage) {
            self.imageView.sd_setImage(with: url, placeholderImage: placeholder)
        }
    }

    // MARK: - PRIVATE METHODS -

    @IBAction private func itemTapped(_ sender: AnyObject) {
        guard let photo = self.photo else { return }
        self.tapped?(photo)
    }

}
