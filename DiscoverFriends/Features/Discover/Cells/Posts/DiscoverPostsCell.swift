//
//  DiscoverPostsCell.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

class DiscoverPostsCell: UITableViewCell {

    // MARK: - VARIABLES -

    @IBOutlet weak var collectionView: UICollectionView!
    var posts: [Post]?
    var tapped:((Post)->())?

    // MARK: - OVERRIDE METHODS -

    override func awakeFromNib() {
        super.awakeFromNib()

        self.registerCells()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.tapped = nil
    }

    // MARK: - PUBLIC METHODS -

    func setup(with posts: [Post]?) {
        self.posts = posts
        self.collectionView.reloadData()
    }
    
}
