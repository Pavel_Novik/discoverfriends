//
//  PostCollectionViewCell.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

class PostCollectionViewCell: UICollectionViewCell {

    // MARK: - CONSTANTS -

    static let kCellIdentifier = "PostCollectionViewCellIdentifier"
    static let kCellName = "PostCollectionViewCell"

    // MARK: - VARIABLES -

    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    fileprivate var post: Post?
    var tapped:((Post)->())?

    // MARK: - OVERRIDE METHODS -

    override func prepareForReuse() {
        super.prepareForReuse()

        self.tapped = nil
    }

    // MARK: - PUBLIC METHODS -

    func setup(with post: Post?) {
        self.post = post

        self.titleLabel.text = self.post?.title ?? ""
        self.bodyLabel.text = self.post?.body ?? ""
    }

    // MARK: - PRIVATE METHODS -

    @IBAction private func itemTapped(_ sender: AnyObject) {
        guard let post = self.post else { return }
        self.tapped?(post)
    }
    
}
