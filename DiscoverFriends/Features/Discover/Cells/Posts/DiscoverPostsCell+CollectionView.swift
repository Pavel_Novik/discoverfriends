//
//  DiscoverPostsCell+CollectionView.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

extension DiscoverPostsCell: UICollectionViewDelegate, UICollectionViewDataSource {

    // MARK: - PUBLIC METHODS -

    func registerCells() {
        let itemCell = UINib.init(nibName: PostCollectionViewCell.kCellName, bundle: nil)
        self.collectionView.register(itemCell, forCellWithReuseIdentifier: PostCollectionViewCell.kCellIdentifier)
    }

    // MARK: - DELEGATES/DATASOURCE METHODS -

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PostCollectionViewCell.kCellIdentifier,
                                                         for: indexPath) as? PostCollectionViewCell {
            cell.setup(with: self.posts?[indexPath.row])
            cell.tapped = self.tapped
            return cell
        }
        return UICollectionViewCell()
    }
    
}
