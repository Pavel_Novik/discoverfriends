//
//  DiscoverVC.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/3/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class DiscoverVC: TopVC {

    // MARK: - VARIABLES -

    @IBOutlet weak var tableView: UITableView!
    var activeSections: [DiscoverSectionsType] = [.photos, .posts]
    var photos: [Photo]?
    var posts: [Post]?

    // MARK: - OVERRIDE METHODS -

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "DISCOVER"
        self.screenName = .discover

        self.setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.refreshViewIfNeeded()
    }

    // MARK: - PUBLIC METHODS -

    func refreshViewIfNeeded() {

        // show for the first time only
        if photos == nil && posts == nil {
            SVProgressHUD.show()
        }
        self.getData { [weak self] in
            self?.tableView.reloadData()
            SVProgressHUD.dismiss()
        }
    }

}
