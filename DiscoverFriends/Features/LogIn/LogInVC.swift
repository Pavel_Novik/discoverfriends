//
//  LogInVC.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class LogInVC: TopVC {

    // MARK: - VARIABLES -

    @IBOutlet weak var usernameTextField: UITextField!

    // MARK: - OVERRIDE METHODS - 

    override func viewDidLoad() {
        super.viewDidLoad()

        self.screenName = .logIn
    }

    // MARK: - PRIVATE METHODS -

    @IBAction fileprivate func logInTapped(_ sender: Any) {
        guard let username = self.usernameTextField.text else { return }
        guard let userId = Int(username) else { return }

        SVProgressHUD.show()
        SessionManager.sharedInstance.loadUser(for: userId) {
            SVProgressHUD.dismiss()
        }
    }
    
}
