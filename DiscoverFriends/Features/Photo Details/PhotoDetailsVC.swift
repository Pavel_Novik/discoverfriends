//
//  PhotoDetailsVC.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit

class PhotoDetailsVC: TopVC {

    // MARK: - VARIABLES -

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var photo: Photo?

    // MARK: - OVERRIDE METHODS -

    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshView()
    }

    // MARK: - PRIVATE METHODS -

    fileprivate func refreshView() {
        self.titleLabel.text = self.photo?.title ?? ""

        if let url = self.photo?.url,
            let placeholder = UIImage(named: kPlaceholderImage) {
            self.imageView.sd_setImage(with: url, placeholderImage: placeholder)
        }
    }

    @IBAction fileprivate func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
