//
//  AccountVC.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class AccountVC: TopVC {

    // MARK: - VARIABLES -

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    // MARK: - OVERRIDE METHODS -

    override func viewDidLoad() {
        super.viewDidLoad()

        self.screenName = .account
        self.setupMapView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateMapView()
        self.refreshUserInfo()
    }

    // MARK: - PRIVATE METHODS -

    @IBAction fileprivate func logOutTapped(_ sender: Any) {
        SessionManager.sharedInstance.logOut()
    }

    fileprivate func refreshUserInfo() {
        let user = SessionManager.sharedInstance.user

        self.emailLabel.text = user?.email?.lowercased() ?? ""
        self.phoneNumberLabel.text = user?.phoneNumber ?? ""
        self.nameLabel.text = user?.name ?? ""
        self.userNameLabel.text = user?.userName ?? ""
        self.addressLabel.text = user?.address?.formattedAddress() ?? ""
    }

}
