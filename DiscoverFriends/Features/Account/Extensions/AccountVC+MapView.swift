//
//  AccountVC+MapView.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import MapKit

extension AccountVC {

    // MARK: - PUBLIC METHODS -

    func setupMapView() {
        self.mapView.mapType = .standard
    }

    func updateMapView() {
        guard let address = SessionManager.sharedInstance.user?.address else { return }
        guard let latitude = address.latitude else { return }
        guard let longitude = address.longitude else { return }

        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)

        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)

        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        self.mapView.addAnnotation(annotation)
    }
    
}
