//
//  PostDetailsVC.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import UIKit
import CLTypingLabel

class PostDetailsVC: TopVC {

    // MARK: - VARIABLES -

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    var post: Post?

    // MARK: - OVERRIDE METHODS -

    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshView()
    }

    // MARK: - PRIVATE METHODS -

    fileprivate func refreshView() {
        self.titleLabel.text = self.post?.title ?? ""
        self.bodyLabel?.text = self.post?.body ?? ""
    }

    @IBAction fileprivate func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
