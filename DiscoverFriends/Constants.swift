//
//  Constants.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation

let kDebugEnabled = true
let kRequestRetryCount = 2
let kHostName = "https://jsonplaceholder.typicode.com/"

// MARK: Notifications name

let kNotificationSessionChanged = "SessionChanged"

// MARK: Images

let kPlaceholderImage = "placeholder"

// MARK: - ENUMS -

enum ScreenNameType: String {
    case account = "Account"
    case discover = "Discover"
    case logIn = "LogIn"

    func name() -> String {
        return self.rawValue
    }
}
