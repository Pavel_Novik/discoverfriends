//
//  Address.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import SwiftyJSON

class Address {

    // MARK: - VARIABLES -

    var latitude: Double?
    var longitude: Double?
    var street: String?
    var suite: String?
    var city: String?
    var zipCode: String?

    // MARK: - INIT -

    init?(json: JSON?) {
        guard let json = json else { return nil }

        if let lat = json["geo"]["lat"].string {
            self.latitude = Double(lat)
        }
        if let lat = json["geo"]["lng"].string {
            self.longitude = Double(lat)
        }
        self.street = json["street"].string
        self.suite = json["suite"].string
        self.city = json["city"].string
        self.zipCode = json["zipcode"].string
    }

    // MARK: - PUBLIC METHODS -

    func formattedAddress() -> String {
        return "\(self.street ?? "") \(self.suite ?? "")\n\(self.city ?? ""), \(self.zipCode ?? "")"
    }

}
