//
//  Photo.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/5/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import SwiftyJSON

class Photo {

    // MARK: - VARIABLES -

    var id: Int?
    var title: String?
    var url: URL?
    var thumbnailUrl: URL?

    // MARK: - INIT -

    init?(json: JSON?) {
        guard let json = json else { return nil }
        guard let id = json["id"].int else { return nil }

        self.id             = id
        self.title          = json["title"].string
        self.url            = json["url"].url
        self.thumbnailUrl   = json["thumbnailUrl"].url
    }

}
