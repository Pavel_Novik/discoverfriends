//
//  User.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {

    // MARK: - VARIABLES -

    var id: Int?
    var name: String?
    var userName: String?
    var address: Address?
    var company: Company?
    var email: String?
    var phoneNumber: String?

    // MARK: - INIT -

    init?(json: JSON?) {
        guard let json = json else { return nil }
        guard let id = json["id"].int else { return nil }

        self.id       = id
        self.name     = json["name"].string
        self.userName = json["username"].string
        self.address  = Address(json: json["address"])
        self.company  = Company(json: json["company"])
        self.phoneNumber = json["phone"].string
        self.email = json["email"].string
    }

}
