//
//  Company.swift
//  DiscoverFriends
//
//  Created by Pavel Novik on 8/4/17.
//  Copyright © 2017 Novik. All rights reserved.
//

import Foundation
import SwiftyJSON

class Company {

    // MARK: - VARIABLES -

    var name: String?

    // MARK: - INIT -

    init?(json: JSON?) {
        guard let json = json else { return nil }
        guard let name = json["name"].string else { return nil }

        self.name = name
    }
    
}
